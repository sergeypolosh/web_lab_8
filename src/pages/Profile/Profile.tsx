import { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { User } from '../../common/types';

export function Profile() {
  const [user, setUser] = useState<User | null>(null);
  const { id } = useParams();

  const fetchUser = async () => {
    if (!id) {
      return;
    }

    /**
     * const user = API CALL GET USER BY ID
     */
  };

  useEffect(() => {
    /**
     * По аналогии с постом, слушаем изменение ID в url-е и вызываем
     *  fetchUser на каждое изменение ID
     */
  }, [id]);

  if (!user) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <h1>{user.name}</h1>
      <p>Phone: {user.phone}</p>
      <p>Email: {user.email}</p>
    </div>
  );
}
